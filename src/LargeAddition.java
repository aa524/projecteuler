//add two large numbers taken in as strings and returned as strings
public class LargeAddition {

	public static void main(String[] args) {
		System.out.println(largeAddition("128", "128"));
		
	}
	
	public static String largeAddition(String a, String b) {
		int length1= a.length();
		int length2= b.length();
		String holder="";
		int front1=0;

	//adding zeros to the front of the shorter number so that addition can take place in all digits
		if (length1 < length2) {
			for (int i=0; i<length2-length1; i++) {
				a= "0" +a;
			}
		}
		else if (length2 < length1) {
			for (int i=0; i<length1-length2; i++) {
				b="0" +b;
			}
		}
		int front=0;
		for (int i=a.length(); i>0; i--) {
		
			int temp=0;
			temp= Integer.parseInt(a.substring(i-1,i)) + Integer.parseInt(b.substring(i-1,i)) + front;
			if (temp<10) {
				holder=temp+holder;
				front=0;
			}
			else if (i==1 && temp >= 10 && temp < 100) {

				holder=temp%10+holder;
				front1=temp/10;
			}
			else if (temp >= 10 && temp < 100) {
				holder=temp%10+holder;
				front=temp/10;
			}
		}
		if (front1 != 0) {
		return front1 + holder;
		}
		else {
		return holder;
		}
	}
	
	public static String digitAdder (String s) {
		int length= s.length();
		int sum=0;
		String holder="";
		for (int i=0; i<length; i++) {
			sum+= Integer.parseInt(s.substring(i,i+1));
		}
		return holder+sum;
		
	}
}
