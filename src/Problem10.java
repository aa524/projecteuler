import java.util.Arrays;


public class Problem10 {

//Problem Description: Summation of primes (Problem 10) 
//The sum of the primes below 10 is 2 + 3 + 5 + 7 =17.
//Find the sum of all the primes below two million.
	
//Use Sieve of Eratosthenes to generate all primes under 2000000 and sum them up at the same time.
//Copying and changing code from Problem3
//INEFFICIENT. Instead of storing all primes and then summing them, sum as you generate them.
	
	public static void main(String[] args) {
		System.out.println(primeSum(2000000));
	}
	
	
	public static double primeSum(double d) {
		double sum=0;
		for (double i=2; i<=d; i++) {
			if (isPrime(i)) {
				sum+=i;
			}
		}
		return sum;
	}
	
	public static boolean isPrime(double d) {
		boolean prime=true;
		for (double i=2; i<=Math.ceil(Math.sqrt(d));i++) {
			if (d%i==0 && d!=i) {
				prime=false;
				return prime;
			}
		}
		return prime;
	}
	
	
	public static void sieveOfEratosthenes(double x) {
		Double[] holder1= new Double[(int)x-2];


		//filling up array holder1 with all values up to the number x
		for (int i=2; i<x; i++) {
			holder1[i-2]=(double) i;
		}
		//Sieve of Eratosthenes to find all primes up to x.
		for (int k=0; k<holder1.length; k++) {
			for (double j=2; j<x; j++) {
				if (holder1[k]%j==0 && holder1[k]!=j) {
					holder1[k]=0.0;
				}
			}
		}

		Arrays.sort(holder1);
		int check= Arrays.binarySearch(holder1, 2.0);
		Double[] holder2= Arrays.copyOfRange(holder1, check, holder1.length);

		double currentcount=0;
		for (int k=0; k<holder2.length; k++){
			currentcount+=holder2[k];
		}
		System.out.println(currentcount);

	}

}
