
public class Problem5 {


// Problem Description: Smallest Multiple (Problem 5)
//	2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
//	What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
	
// I solved this problem by paper. I listed all the primes under 20, then tried to make every number from 1 to 20
// from a combination of these. If I couldn't, I multiplied in more primes as necessary. i.e. the primes from 1 to 20 are
// 2, 3, 5, 7, 11, 13, 17, 19. 1 divides into all numbers. 2 and 3 are already there. 4 cannot be made from the numbers so far,
// so I included another 2. 5 is already there. 6 is 2*3, which is already there. 7 is there. 8 needs another two, so I added that in.
// So on and so forth. I had to include another 3 to get a 9, and another 2 to get a 16. Then multiply it all out and that is the answer.

}