
public class Problem6 {
//  Problem Description: Sum square difference (Problem 6)
//	The sum of the squares of the first ten natural numbers is, 1^2 + 2^2 + ... + 10^2 = 385.
//	The square of the sum of the first ten natural numbers is, (1+2+... 10)^2 = 55^2 = 3025.
//  Hence the difference between the sum of the squares of the first ten natural numbers and 
//	the square of the sum is 3025 - 385 = 2640. Find the difference between the sum of the squares 
//	of the first one hundred natural numbers and the square of the sum.


	public static void main(String[] args) {
		double sum= sumOfSquares(100D);
		double square= squareOfSum(100D);
		System.out.println(square-sum);

	}

	public static double sumOfSquares(double d) {
		double sum=0;
		for (double i=1; i<=d; i++) {
			sum+= i*i;
		}
		return sum;
	}
	
	public static double squareOfSum(double d) {
		double sum=0;
		//This part can be done quickly in your head using Gauss's childhood trick
		//e.g. sum from 1 to 100 can be done as 1 + 100, 2+99, 3+98, etc, which is 50*101
		
		for (double i=1; i<=d; i++) {
			sum+=i;
		}
		return sum*sum;
	}
}
