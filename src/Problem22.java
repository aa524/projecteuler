import java.io.BufferedReader;
import java.util.Arrays;
import java.io.File;
import java.io.FileReader;
import java.io.*;

class Problem22 {
	/**Using names.txt, a 46K text file containing over five-thousand first names, begin by
	 * sorting it into alphabetical order. Then working out the alphabetical value for each name,
	 * multiply this value by its alphabetical position in the list to obtain a name score.
	 * For example, when the list is sorted into alphabetical order, COLIN, which is worth
	 * 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score
	 * of 938 × 53 = 49714.
	 * What is the total of all the name scores in the file?
	 */

	//Use path to names.txt as an CLI when running Problem22
	public static void main(String[] args) throws IOException {
		File f= new File(args[0]);
		System.out.println(computeFileValue(f));
	}
	
	private static long computeFileValue(File f) throws IOException, FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(f));
		StringBuilder str= new StringBuilder();
		while (br.ready()) {
		str.append(br.readLine());
		}
		String[] names= str.toString().split("\",\"");
		//The first word will have a prefix of a quotation mark. This will remove it
		names[0]= names[0].substring(1);
		//The last word will have a suffix of a quotation mark. This will remove it
		names[names.length-1] = names[names.length-1].substring(0, names[names.length-1].length() - 1);
		Arrays.sort(names);
		int count=1;
		long sum=0;
		for (String n: names) {
			sum+= count * computeNameScore(n);
			count++;
		}
		return sum;
		
	}
	
	private static int computeNameScore(String s) {
		int length = s.length();
		String str= s.toLowerCase();
		int ASCIIValueForAMinus1= 96;
		int score=0;
		for (int i=0; i<length; i++) {
			score+= (int) (str.charAt(i) - ASCIIValueForAMinus1);
		}
		return score;
	}
	
	
}
