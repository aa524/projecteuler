import java.util.*;
import java.math.*;
public class Problem21 {
//Problem Description: Amicable Numbers (Problem 21)
//Let d(n) be defined as the sum of proper divisors of n (numbers less than n
//which divide evenly into n). If d(a)=b and d(b)=a, where a != b, then a and b
//are an amicable pair and each of a and b are called amicable numbers.
//For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110;
//therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71, and 142; so d(284) = 220.
//Evaluate the sum of all the amicable numbers under 10,000.
	
//Two approaches: 1.) Brute force. Compute the sum of the proper divisors for each number under
//10000. Store in a array with the index being the number and the value being d(number). Then
//check if array[array[index]] == index. If so, sum up all pairs.
//2.) Use Euler's Rule, which states, if:
// p=(2^(n-m) + 1) x 2^(m) - 1,
// q=(2^(n-m) + 1) x 2^(n) - 1,
// r=(2^(n-m) + 1)^(2) x 2^(m+n) - 1,
// where n > m > 0 are integers and p,q, and r are prime numbers, then 2^(n) x p x q and 2^(n) x r are
// a pair of amicable numbers.
// This method doesn't seem to generate all pairs.
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// amicableNumbers();
		
		System.out.println(sumOfAmicableNumbers(10000));

	}
	
	public static long sumOfAmicableNumbers(int n) {
		int [] holder= new int[n+1];
		for (int i=0; i<=n; i++) {
			holder[i]=sumOfProperDivisors(i);
			System.out.println(i + " " + sumOfProperDivisors(i));
		}
		long sum=0;
		for (int i=0; i<holder.length; i++) {
			if (holder[i]<holder.length) {
			if (i==holder[holder[i]] && (i!=holder[i])) {
				System.out.println(holder[i] + " " + holder[holder[i]] + " " + i);
				sum+=holder[i];
			}
			}
		}
		return sum;
	}
	
	public static int sumOfProperDivisors(int n) {
		double squareroot= Math.sqrt(n);
		int sum=0;
		for (int i=2; i<= (int) squareroot; i++) {
			if (n%i==0) {
				sum+=i;
				if ((n/i)!=i) {
					sum+=(n/i);
				}
			}
		}
		return sum + 1;
	}
	
	public static ArrayList<Integer> amicableNumbers() {
		ArrayList<Integer> holder= new ArrayList<Integer>();
		//since an amicable number is of the form 2^(n) x r, where r is a prime number (thus, greater than 1)
		//n only needs to go up to 15, since 2^14 is 16,384 and that is greater than 10,000.
		for (int m = 1; m < 14; m++) {
			for (int n= 2; n < m && n<15; n++) {
				//p=(2^(n-m) + 1) x 2^(m) - 1
				double p= (Math.pow(2,n-m) + 1)*Math.pow(2, m)-1;
				//q=(2^(n-m) + 1) x 2^(n) - 1
				double q= (Math.pow(2,n-m) + 1)*Math.pow(2, n)-1;
				//r=(2^(n-m) + 1)^(2) x 2^(m+n) - 1
				double r= Math.pow((Math.pow(2,n-m)+ 1),2)*Math.pow(2, m+n) - 1;
				if(Problem7.isPrime((int)p) && Problem7.isPrime((int)q) && Problem7.isPrime((int)r)) {
				double amicableNumber1= Math.pow(2,n)*p*q;
				double amicableNumber2= Math.pow(2, n)*r;
				holder.add((int)amicableNumber1);
				holder.add((int)amicableNumber2);
				};
			}
		}
		for (int i: holder) {
			System.out.println(i);
		}
		return holder;
	}

}
