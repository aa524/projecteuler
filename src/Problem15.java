
public class Problem15 {
//Problem Description: Lattice Paths (Problem 15)
//Starting in the top left corner of a 2x2 grid, and only being able to move to the right and down,
//there are exactly 6 routes to the bottom right corner.
//How many such routes are there through a 20x20 grid?
	
//I wrote my solution on paper first (thought of solution while going to sleep). I put dots on the paper 
//for each grid intersection, then wrote the number of paths that would be changed by that dot,
//viz., if a path was heading down and the dot could create a new path by making a path to the left, or vice-versa
//then I would increment it. I noticed a pattern after doing a 3x3 square. The pattern was described by the
//equation f(x,y)=f(x-1,y) + f(x,y-1), for 0<=x<=20 and 0<=y<=20, and f(0,0)=2.
	public static void main(String[] args) {
		long [][] holder= new long[20][20];
		holder[0][0]=2;
		for (int i=1; i<20; i++) {
			holder[0][i]=1;
			holder[i][0]=1;
		}
		for (int i=1; i<20; i++) {
			for (int j=1; j<20; j++) {
				holder[j][i]=holder[j-1][i] + holder[j][i-1];			}
		}
		
		long count= 0;
		for (int i=0; i<20; i++) {
			for (int j=0; j<20; j++) {
				System.out.print(holder[j][i]+ " ");
				count+=holder[j][i];
			}
			System.out.println();
		}
		System.out.println(count);
			

	}

}
