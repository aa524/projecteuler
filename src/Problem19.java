import java.util.*;
public class Problem19 {
//Problem Description: Counting Sundays (Problem 19)
//You are given the following information, but you may prefer to do some research for yourself.
//1 Jan 1900 was a Monday.
//Thirty days has September, April, June, and November. All the rest have thirty-one, Saving February
//alone, which has twenty-eight, rain or shine. And on leap years, twenty-nine.
//A leap year occurs on any year evenly divisible by 4, but not on a century, unless it is divisible
//by 400.
//How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
	
//Do not include any dates from 1900
	public static void main(String[] args) {
		ArrayList<Integer> firstOfMonth= firstOfMonth();
		ArrayList<Integer> sundays= sundays();
		//Number of sundays that fall on the first of a month
		int count=0;
		for (int i: firstOfMonth) {
			//Do not include dates in the year 1900
			if (sundays.contains(i) && i>365) {
				count++;
				System.out.println(i);
			}
		}
		System.out.println(count);

	}
	
	public static ArrayList<Integer> firstOfMonth() {
		int[] regularYear= {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		int[] leapYear= {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		ArrayList <Integer> holder= new ArrayList<Integer>();
		int firstOfMonth=1;
		for (int year=1900; year<2001; year++) {
			//all leap year
			if (year%400==0 || (year%4==0 && year%100 !=0)) {
				for (int month=0; month<12; month++) {
				holder.add(firstOfMonth+leapYear[month]);
				firstOfMonth+=leapYear[month];
				}
			}
			//not leap year
			else {
				for (int month=0; month<12; month++) {
				firstOfMonth+=regularYear[month];
				holder.add(firstOfMonth);
				}
			}
		}
//printing out values of all the first of months
//		for (int i: holder) {
//			System.out.println(i);
//		}
		return holder;
		
	}
	public static ArrayList<Integer> sundays() {
		//If Jan 1 1900 was a Monday, then Jan 7 1900 was a Sunday
		int firstSunday= 7;
		ArrayList<Integer> sundays= new ArrayList<Integer>();
		for (int sunday=firstSunday; sunday<36892; sunday=sunday+7) {
			sundays.add(sunday);
		}
		return sundays;
		
	}
}
