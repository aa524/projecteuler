import java.util.*;

public class Problem3 {
	/*	Problem Description: Largest prime factor (Problem 3)
	The prime factors of 13195 are 5, 7, 13 and 29.
	What is the largest prime factor of the number 600851475143 ?*/

	public static void main(String[] args) {
		System.out.println(primeFactors(600851475143D));
//		sieveOfEratosthenes(400);
	}

	public static void sieveOfEratosthenes(double x) {
		double roundroot= Math.round(Math.sqrt(x));
		Double[] holder1= new Double[(int) roundroot-2];


		//filling up array holder1 with all values up to the root of the number x
		for (int i=2; i<roundroot; i++) {
			holder1[i-2]=(double) i;
		}
		//Sieve of Eratosthenes to find all primes up to root of x.
		for (int k=0; k<holder1.length; k++) {
			for (double j=2; j<roundroot; j++) {
				if (holder1[k]%j==0 && holder1[k]!=j) {
					holder1[k]=0.0;
				}
			}
		}

		Arrays.sort(holder1);
		int check= Arrays.binarySearch(holder1, 2.0);
		Double[] holder2= Arrays.copyOfRange(holder1, check, holder1.length);


		for (int k=0; k<holder2.length; k++) {
			System.out.println(holder2[k]);
		}
	}

	public static double fermatFactor(double d) {
		double a= Math.ceil(Math.sqrt(d));
		double b2= a*a-d;
		while (Math.sqrt(b2) != Math.ceil(Math.sqrt(b2))) {
			a=a+1;
			b2=a*a-d;
		}
		return a + Math.sqrt(b2);
	}

	public static double primeFactors(double d) {
		double currentnumber=d;
		double largestPrime=0;
		//Loop iterator i won't ever reach d since a prime factor will be found before then and returned.
		//So no need to compute square root of d (upper limit of divisors).
		for (double i=2; i< d; i++) {
			while (currentnumber%i==0)
				currentnumber= currentnumber/i;
			if (currentnumber==1) {
				largestPrime=i;
				return largestPrime;

			}
		}
		return largestPrime;
	}
}