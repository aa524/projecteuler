
public class Problem7 {

//Problem Description: 10001st prime (Problem 7)
//By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
//What is the 10,001st prime number?
	
//One way to approach this problem is to have a counter and use the Sieve of Eratosthenes from problem 3
//with a really high number and keep count until 10,001. Is there a more elegant solution?

	
	public static void main(String[] args) {
		boolean c= false;
		int count=0;
		int primeNumber=0;
		for (int i=2; !c; i++) {
			if (isPrime(i)) {
				count++;
				primeNumber=i;
			}
		if (count==10001) {
			c=true;
		}
		}
		System.out.println(primeNumber);

	}
	
	public static boolean isPrime(int p) {
		boolean prime= true;
		int squareroot= (int) Math.floor(Math.sqrt(p));
		for (int i=2; i<= squareroot; i++) {
			if (p%i==0) {
				prime=false;
				return prime;
				
			}
		}
		return prime;
	}
}
