public class Problem4 {

//	Problem Description: Largest palindrome product (Problem 4)
//	A palindromic number reads the same both ways.
//	The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 x 99.
//	Find the largest palindrome made from teh product of two 3-digit numbers.
	
	public static void main(String[] args) {
		//try to find largest int that is a palindrome and a product of two three digit ints
		int largest=0;
		//multiplying all combinations of 3 digit numbers
		for (int i=100; i<1000; i++) {
			for (int j=100; j<1000; j++) {
				int product= i*j;
				String stringNum= Integer.toString(product);
				if (isPalindrome(stringNum)) {
					if (Integer.parseInt(stringNum) > largest) {
						largest=Integer.parseInt(stringNum);
					}
				}
			}
		}
		System.out.println(largest);
	}

	//Checks if a string is a palindrome (case-sensitive, but that doesn't matter for numbers)
	public static boolean isPalindrome(String s) {
		boolean palindrome=true;
		int length= s.length();
		for (int i=0; i<length/2; i++) {
			if (s.charAt(i)!=s.charAt(length-i-1)) {
				palindrome=false;
			}

		}

		return palindrome;
	}

}
