import java.io.*;
import java.util.*;
public class Problem13 {
	//Problem Description: Large sum (Problem 13)
	//Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.
	//numbers put into a file on my desktop
	//I will read in the numbers as a string. Then, since digits only seem to affect two digits over (e.g.
	//in 60538, the maximal effect of a hundred nines in the one's digit place will usually affect the thousands
	//place, and only rarely will it affect the ten thousands place (only if the thousands place has a nine and the
	//hundreds place is above 1).
	//

	public static void main(String[] args) throws FileNotFoundException, IOException{
		String[] holder1= numberReader();
		System.out.println(columnSum(holder1,0));
	}

	public static String[] numberReader() throws FileNotFoundException, IOException{
		BufferedReader filename= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("What is the path to the text file containing the numbers to problem 13?");
		File file= new File(filename.readLine());
		filename.close();
		BufferedReader br= new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		ArrayList<String> stringholder= new ArrayList<String>();
		//Add lines to the arraylist while there exists lines to be read
		while (br.ready()) {
			stringholder.add(br.readLine());
		}
		br.close();
		String[] holder= stringholder.toArray(new String[stringholder.size()]);
		return holder;
	}
	//Finds sum down a "column" of numbers of width (currently:13) in a string array
	public static long columnSum(String[] holder, int column) {
		int size= holder.length;
		//Assume all string numbers have the same number of digits
		//int digits= holder[0].length();
		long sum=0;

		for (int i=0; i<size; i++) {
			sum+=Long.parseLong(holder[i].substring(column, column+13));
		}
		return sum;



	}
}
