import java.io.*;
import java.util.*;
public class Problem67 {

	/**
	 * @param args
	 */
	public static void main (String[] args) throws FileNotFoundException, IOException{
		System.out.println(triangleCascade(triangleImporter()));

	}
	
	public static int[][] triangleImporter() throws FileNotFoundException, IOException{
		int [][] holder= new int[100][100];
		File trianglenumbers= new File("/Users/arunananthamoorthy/Desktop/triangle.txt");
		
		FileInputStream fs= new FileInputStream(trianglenumbers);
		BufferedReader br= new BufferedReader(new InputStreamReader(fs));
		int row=0;
		while (br.ready()) {
			int column=0;
			String s= br.readLine();
			//Using StringTokenizer instead of for loops makes it a lot simpler
			StringTokenizer st= new StringTokenizer(s);
			
			while (st.hasMoreTokens()) {
				holder[row][column]=Integer.parseInt(st.nextToken());
				column++;
			}
			row++;
		}
		br.close();
		return holder;
		
	}
	public static int triangleCascade(int[][] triangle) {
		for (int i=1; i< triangle.length; i++)
			for (int j=0; j<triangle[i].length; j++) {
				//left or right side
				if (j==0) {
					triangle[i][j]+= triangle[i-1][j];
				}
				else if (j==i) {
					triangle[i][j]+= triangle[i-1][j-1];
				}
				else {
					triangle[i][j]+=Math.max(triangle[i-1][j-1],triangle[i-1][j]);
				}
			}
		int max=0;
		for (int i: triangle[99]) {
			//System.out.println(i);
			if (i > max) {
				max=i;
			}
		}
		return max;
		

		
	}

}

