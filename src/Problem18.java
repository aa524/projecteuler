import java.io.*;
public class Problem18 {
//Problem Description: Maximum path sum I (Problem 18)
//By starting at the top of the triangle below and moving to adjacent numbers on the row below,
//the maximum total from top to bottom is 23.
//		3
//	   7 4
//	  2 4 6
//   8 5 9 3
//That is, 3 + 7 + 4 + 9 = 23. Find the maximum total from top to bottom of the triangle below:
	
	public static void main (String[] args) throws FileNotFoundException, IOException{
		System.out.println(triangleCascade(triangleImporter()));

	}
	
	public static int[][] triangleImporter() throws FileNotFoundException, IOException{
		int [][] holder= new int[15][15];
		System.out.println("What is the path for the text file containing the numbers for problem 18?");
		BufferedReader filename= new BufferedReader(new InputStreamReader(System.in));
		File trianglenumbers= new File(filename.readLine());
		filename.close();
		
		FileInputStream fs= new FileInputStream(trianglenumbers);
		BufferedReader br= new BufferedReader(new InputStreamReader(fs));
		int row=0;
		while (br.ready()) {
			int column=0;
			String s= br.readLine();
			for (int i=0; i< s.length()-1; i++) {
				// if (s.charAt(i-1) == ' ' || s.charAt(i+2) == ' ') {
				if (s.indexOf(' ', i) == -1) {
					holder[row][column]=Integer.parseInt(s.substring(i,i+2));
				}
				else if (s.indexOf(' ', i)==i+2) {
					
					holder[row][column]=Integer.parseInt(s.substring(i,i+2));
					column++;
				}
			}
			row++;
			
		}
		br.close();
//		for (int[] h: holder) {
//			for (int i: h) {
//				System.out.println(i);
//			}
//		}
//		System.out.println(holder[1][0]);
		return holder;
		
	}
	public static int triangleCascade(int[][] triangle) {
		for (int i=1; i< triangle.length; i++)
			for (int j=0; j<triangle[i].length; j++) {
				//left or right side
				if (j==0) {
					triangle[i][j]+= triangle[i-1][j];
				}
				else if (j==i) {
					triangle[i][j]+= triangle[i-1][j-1];
				}
				else {
					triangle[i][j]+=Math.max(triangle[i-1][j-1],triangle[i-1][j]);
				}
			}
		int max=0;
		for (int i: triangle[14]) {
		//	System.out.println(i);
			if (i > max) {
				max=i;
			}
		}
		return max;
		

		
	}

}
