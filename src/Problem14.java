
public class Problem14 {
//Problem Description: Longest Collatz sequence (Problem 14)
//The following iterative sequence is defined for the set of positive integers:
//		n -> n/2 (n is even)
//		n -> 3n + 1 (n is odd)
//Using the rule above and starting with 13, we generate the following sequence:
//		13 -> 40 -> 20 -> 10 -> 5 -> 16 -> 8 -> 4 -> 2 -> 1
//It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it
//has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
//
//Which starting number, under one million, produces the longest chain?
//NOTE: Once the chain starts the terms are allowed to go above one million.
	
	public static void main(String[] args) {
		System.out.println(numbersUpToMillion());
		System.out.println(numberOfTerms(13l));
		
	}
	
	//checks for longest Collatz sequence in first million numbers
	private static long numbersUpToMillion() {
		//longest sequence
		int longest=0;
		//number with the longest sequence
		long largest=0;
		for (long i=0; i<= 1000000; i++) {
			if (numberOfTerms(i)>longest) {
				longest=numberOfTerms(i);
				largest=i;
			}
		}
		return largest;
	}
	
	private static int numberOfTerms(long n) {
		int counter=0;
		while (n > 1) {
			if (n%2==0) {
				//bit shift to the right by one bit. equivalent to dividing by two.
				n= n >> 1;
			}
			else {
				n=3*n+1;
			}
			counter++;
		}
		return counter+1;
	}
}
