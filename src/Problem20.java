public class Problem20 {
//Problem Description: Factorial Digit Sum (Problem 20)
//n! means n x (n - 1) x ... x 3 x 2 x 1
//For example, 10! = 10 x 9 x ... x 3 x 2 x 1 = 3628800,
//and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
//Find the sum of the digits in the number 100!
	
	public static void main(String[] args) {
		System.out.println(LargeAddition.digitAdder(multiplier()));
	}
	
	public static String multiplier() {
		//4x6=6+6+6+6=((6+6)+6)+6. Holder will hold the sum of the numbers within the parenthesis, while
		//previous will hold the 6, and once the addition is complete and holder equals 24, previous will
		//take on that value (24). 
		String holder="2";
		String previous="2";
		for (int j=2; j<100; j++) {
			for (int i=0; i<j; i++) {
			holder=LargeAddition.largeAddition(previous, holder);
			}
		previous=holder;
		}
		return holder;
	}
}