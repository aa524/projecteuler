
public class Problem9 {
// Problem Description: Special Pythagorean triplet (Problem 9)
// A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
// a^2 + b^2 = c^2
// For example, 3^3 + 4^2 = 9 + 16 = 25 = 5^2.
// There exists exactly one Pythagorean triplet for which a + b + c= 1000.
// Find the product abc.

// From wikipedia, all Pythagorean triplets can be constructed by selecting an m and an n from
// the set of natural numbers with m > n, and setting a=m^2 - n^2, b=2mn, and c=m^2 + n^2.
// Given that a + b + c = 1000, and substituting in, we get m^2 + nm - 500= 0;
	
	public static void main(String[] args) {
		boolean found=false;
		int mholder=0;
		int nholder=0;
		for (int n=0; !found && n < 1000; n++) {
			for (int m=0; !found && m < 1000; m++) {
				if ((m > n) && (m*m + m*n - 500 == 0)) {
					mholder=m;
					nholder=n;
					found=true;
				}
			}
		}
		System.out.println("m is " + mholder + " and n is " + nholder);
		System.out.println("a is " + (mholder*mholder - nholder*nholder));
		System.out.println("b is " + (2*mholder*nholder));
		System.out.println("c is " + (mholder*mholder + nholder*nholder));
		System.out.println("Product abc is " + (375*200*425));

	}
	


}
