import java.util.*;


public class Problem17 {

//Problem Description: Number Letter Counts (Problem 17)
//If the numbers 1 to 5 are written out in words: one, two, three, four, five, then 
//there are 3+3+5+4+4=19 letters in total.
//If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words,
//how many letters would be used?
//NOTE: Do not count spaces or hyphens. For example, 343 (three hundred and forty-two) contains
//23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing
//out numbers is in compliance with British usage.

//Use arrays or hash map to map numbers to their word representations. Make sure to include teens
//and eleven and twelve. Use modulo and divisions to find out what the digits are.
	
	public static void main(String[] args) {
		System.out.println(letterCounter());
		
	}
	
	public static int letterCounter() {
		int counter=0;
		for (int i=1; i<1001; i++) {
			for (int j=0; j<numberToText(i).length(); j++) {
				if (!numberToText(i).substring(j,j+1).equals(" ")) {
					counter++;
				}
			}
		}
		return counter;
	}
	
	//converts number such as 12 to "twelve".
	public static String numberToText(int i) {
		String numbertext="";
		Boolean hasHundred=false;
		HashMap <Integer, String> numbers= new HashMap<Integer, String>();
		numbers.put(0, "");
		numbers.put(1, "one");
		numbers.put(2, "two");
		numbers.put(3, "three");
		numbers.put(4, "four");
		numbers.put(5, "five");
		numbers.put(6, "six");
		numbers.put(7, "seven");
		numbers.put(8, "eight");
		numbers.put(9, "nine");
		numbers.put(10, "ten");
		numbers.put(11, "eleven");
		numbers.put(12, "twelve");
		numbers.put(13, "thirteen");
		numbers.put(14, "fourteen");
		numbers.put(15, "fifteen");
		numbers.put(16, "sixteen");
		numbers.put(17, "seventeen");
		numbers.put(18, "eighteen");
		numbers.put(19, "nineteen");
		numbers.put(20, "twenty");
		numbers.put(30, "thirty");
		numbers.put(40, "forty");
		numbers.put(50, "fifty");
		numbers.put(60, "sixty");
		numbers.put(70, "seventy");
		numbers.put(80, "eighty");
		numbers.put(90, "ninety");
		numbers.put(100, "hundred");
		numbers.put(1000, "one thousand");
		
		//if number is 1000
		if (i==1000) {
			numbertext=numbers.get(i);
		}
		//isolating hundreds digit
		if (i/100 != 0 && i/100!=10) {
			numbertext+=numbers.get(i/100) + " hundred";
			hasHundred=true;
		}
		//isolating 10s and 1s digit
		if ((i%100 > 19)) {
			if (hasHundred) {
				numbertext+= " and ";
			}
			numbertext+=numbers.get(((i/10)%10)*10) + " " + numbers.get(i%10);
		}
		//isolating 10s and 1s digit
		if (i%100 < 20 && i%100!=0) {
			if (hasHundred) {
				numbertext+= " and ";
			}
			numbertext+=numbers.get(i%100);
		}
		return numbertext;
		
	}
}