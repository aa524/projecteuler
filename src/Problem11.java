import java.io.*;
import java.util.*;
public class Problem11 {
	// Problem Description: Largest product in a grid (Problem 11)
	// In the 20x20 grid below, four numbers along a diagonal line have been marked in red.
	// Grid not shown
	// The product of these numbers is 26 x 63 x 78 x 14 - 1788696
	// What is the greatest product of four adjacent numbers in the same direction (up, down, left, right,
	// or diagonally) in the 20x20 grid?

	//First I will import the grid into a 20x20 array from a txt file.
	public static void main(String[] args) throws FileNotFoundException, IOException{
	BufferedReader filename= new BufferedReader(new InputStreamReader(System.in));
	System.out.println("What is the path to the text file containing the grid for problem 11?");
	File file= new File(filename.readLine());
	filename.close();
	BufferedReader br= new BufferedReader(new InputStreamReader(new FileInputStream(file)));
	List<int[]> holder= new ArrayList<int[]>();
	while (br.ready()) {
		holder.add(stringToArray(br.readLine(), 20));
	}
	
	br.close();
	
	int[][] grid= new int[][] {holder.get(0), holder.get(1), holder.get(2), holder.get(3), holder.get(4), holder.get(5), holder.get(6), holder.get(7),
			holder.get(8), holder.get(9), holder.get(10), holder.get(11), holder.get(12), holder.get(13), holder.get(14), holder.get(15), holder.get(16), holder.get(17), holder.get(18),
			holder.get(19)};
	
	
//	for (int i=0; i<20; i++) {
//		for (int j=0; j<20; j++) {
//			System.out.println(grid[i][j]);
//		}
//		System.out.println("row number is " + i);
	System.out.println(multiplier(grid));
	}
	
	
	

	
	public static int multiplier(int[][] grid) {
		int maxProduct=0;
		//going vertically, with i as X position and j as X position
		for (int i=0; i<20; i++){
			for (int j=0; j<17; j++) {
				int product= grid[i][j]*grid[i][j+1]*grid[i][j+2]*grid[i][j+3];
				if (product > maxProduct) {
					maxProduct=product;
				}
			}
		}
		
		//going horizontally (should be similar to horizontally, except with i and j switched)
		
		for (int x=0; x<17; x++) {
			for (int y=0; y<20; y++) {
				int product=grid[x][y]*grid[x+1][y]*grid[x+2][y]*grid[x+3][y];
				if (product>maxProduct) {
					maxProduct=product;
				}
			}
			}
				
		
		//going diagonally like a forward slash: /
		for (int x=3; x<20; x++) {
			for (int y=0; y<17; y++) {
				int product= grid[x][y]*grid[x-1][y+1]*grid[x-2][y+2]*grid[x-3][y+3];
				if (product>maxProduct) {
					maxProduct=product;
				}
			}
		}
		
		//going diagonally like a backward slash: \
		for (int x=0; x<17; x++) {
			for (int y=0; y<17; y++) {
				int product= grid[x][y]*grid[x+1][y+1]*grid[x+2][y+2]*grid[x+3][y+3];
				if (product>maxProduct) {
					maxProduct=product;
				}
			}
		}
		
		return maxProduct;
	}
	
	
	//Takes a string of characters which are numbers separated by a space and puts it into an array. 
	//Assumes only one space between each number. 
	public static int[] stringToArray(String s, int n) {
		String trimmed=s.trim();
		int length= trimmed.length();
		int counter=0;
		int lastspace=0;
		int[] intArray= new int[n];
		for (int i=0; i<length; i++) {
			if (trimmed.charAt(i)==' ') {
				intArray[counter]=Integer.parseInt(trimmed.substring(lastspace, i).trim());
				lastspace=i;
				counter++;
			}
			else if (i==length-1) {
				intArray[counter]=Integer.parseInt(trimmed.substring(lastspace, i+1).trim());
			}
		}
		return intArray;

	}
}